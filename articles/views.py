
from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.parsers import FormParser, MultiPartParser
from django.core.cache import cache

from .serializers import ArticleSerializer, CommentSerializer, UserMiniSerializer, UserSerializer
from .models import Article, Comment

class UserViewSet(ModelViewSet):
    def get_queryset(self):
        if cache.get('queryset'):
            queryset = cache.get('queryset')
        else:
            queryset = User.objects.all()
            cache.set('queryset', queryset, 4 * 60)
        return queryset
    
    def get_serializer_class(self):
        if self.action == 'list':
            return UserMiniSerializer
        return UserSerializer
    
    permission_classes_by_action = {'retrieve': [IsAuthenticated],
                                    'list': [],
                                    'create': [IsAuthenticated],}
    
    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]
    
    parser_classes = [FormParser, MultiPartParser]


class ArticleViewSet(ModelViewSet):
    queryset = Article.objects.select_related('related_user').all()
    serializer_class = ArticleSerializer
    parser_classes = [FormParser, MultiPartParser]
    permission_classes = [IsAuthenticated]

class CommentViewSet(ModelViewSet):
    queryset = Comment.objects.filter(is_active=True)
    serializer_class = CommentSerializer
    parser_classes = [FormParser, MultiPartParser]