from django.contrib.admin import site, ModelAdmin, TabularInline, StackedInline
from .models import Article, Comment


class CommentInline(TabularInline):
    model = Comment

class ArticleAdmin(ModelAdmin):
    list_display = ['id', 'title', 'is_active', 'related_user']
    inlines = [
        CommentInline
    ]

site.register(Article, ArticleAdmin)
