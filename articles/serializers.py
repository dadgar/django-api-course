from django.contrib.auth.models import User
from django.core.cache import cache
from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Article, Comment


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        exclude = ['password']

class UserMiniSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username']


class ArticleSerializer(ModelSerializer):
    related_user = UserMiniSerializer(read_only=True)
    authors = UserMiniSerializer(many=True, read_only=True)
    comments_count = SerializerMethodField()
    class Meta:
        model = Article
        fields = '__all__'
    
    def get_comments_count(self, obj):
        cache_key = f'{obj.id}_comments_count'
        if cache.get(cache_key):
            return cache.get(cache_key)
        cache.set(cache_key, obj.comment_set.all().count(), 15 * 60)
        return cache.get(cache_key)

class CommentSerializer(ModelSerializer):
    related_article = ArticleSerializer(read_only=True)
    related_user = UserMiniSerializer(read_only=True)
    class Meta:
        model = Comment
        fields = '__all__'